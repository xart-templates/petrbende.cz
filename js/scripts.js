jQuery.noConflict();
jQuery(document).ready(function($){



	$('html').addClass('js');



	$('.toggle_menu').on('click',function(){
		$('body').toggleClass('menu-on');
	});



	$('.detail_image').each(function(){
		var i = $('li',this).length;
		$('ul',this).wrapAll('<div class="slider"/>');
		var clone = $('.slider ul',this).clone().addClass('thumbnails');
		$('.slider',this).after(clone);
		if(i>1){
			$('.slider',this).append('<a class="prev">&lt;</a><a class="next">&gt;</a>');
			$('.slider ul',this).cycle({
				fx: 'scrollHorz',
				slides: 'li',
				speed: 200,
				timeout: 0,
				swipe: true,
				prev: $('.prev',this),
				next: $('.next',this),
				pager: $('.thumbnails',this),
				pagerTemplate: '',
			});
		}
	});



	$('.mod_custom_panes').each(function(){
		var i = $('.pane',this).length;
		if(i>1){
			$(this).cycle({
				slides: '.pane',
				speed: 1,
				timeout: 0,
				pager: $('.tabs ul',this),
				pagerTemplate: '',
			});
		}
	});


	

	var WindowHeight = $(window).height();
	$('.mod_custom-slideshow').css({'height':WindowHeight,'padding':'0'});
	$(window).resize(function(){
		var WindowHeight = $(window).height();
		$('.mod_custom-slideshow').css({'height':WindowHeight,'padding':'0'});
	});

	$('.mod_custom-slideshow').each(function(){
		
		$('.layer_01', this).each(function(){
			var img = $('img',this).attr('src');
			$('img',this).hide();
			$(this).css('backgroundImage','url('+img+')');
		});

		var i = $('.item',this).length;
		if(i>1){
			$(this).append('<div class="pager"/>');
			$('.items',this).cycle({
				slides: '.item',
				speed: 600,
				timeout: 9000,
				//pauseOnHover: true,
				swipe: true,
				pager: $('.pager',this),
			});
		}
	});




	$('.mod_custom_calendar').each(function(){
		$('.items',this).after('<a class="prev">&lt;</a><a class="next">&gt;</a>');
		$('.items',this).cycle({
			fx: 'carousel',
			slides: '.item',
			speed: 300,
			timeout: 0,
			swipe: true,
			prev: $('.prev',this),
			next: $('.next',this),
		});
	});



	$('.mod_custom_discography, .mod_custom_favourite').each(function(){
		$('.items',this).after('<a class="prev">&lt;</a><a class="next">&gt;</a>');
		$('.items',this).cycle({
			fx: 'carousel',
			slides: '.item',
			speed: 300,
			timeout: 0,
			swipe: true,
			prev: $('.prev',this),
			next: $('.next',this),
		});
	});


	$('.mod_custom_calendar .item, .eshop_items .item a, .mod_custom_favourite .item a').matchHeight({
		property: 'min-height',
	});


	$('.mod_banners').each(function(){
		$('ul',this).after('<a class="prev">&lt;</a><a class="next">&gt;</a>');
		$('ul',this).cycle({
			fx: 'carousel',
			slides: 'li',
			speed: 300,
			timeout: 0,
			swipe: true,
			prev: $('.prev',this),
			next: $('.next',this),
		});
	});


	


	// file input style
	$('input[type=file]').each(function(){
		var uploadText = $(this).data('value');
		if (uploadText) {
			var uploadText = uploadText;
		} else {
			var uploadText = 'Choose file';
		}
		var inputClass=$(this).attr('class');
		$(this).wrap('<span class="fileinputs"></span>');
		$(this)
			.parents('.fileinputs')
			.append($('<span class="fakefile"/>')
			.append($('<input class="input-file-text" type="text" />')
			.attr({'id':$(this)
			.attr('id')+'__fake','class':$(this).attr('class')+' input-file-text'}))
			.append('<input type="button" value="'+uploadText+'">'));
		$(this)
			.addClass('type-file')
			.css('opacity',0);
		$(this)
			.bind('change mouseout',function(){$('#'+$(this).attr('id')+'__fake')
			.val($(this).val().replace(/^.+\\([^\\]*)$/,'$1'))
		})
	});



	// placeholder
	if (document.createElement('input').placeholder==undefined){
		$('[placeholder]').focus(function(){
			var input=$(this);
			if(input.val()==input.attr('placeholder')){
				input.val('');
				input.removeClass('placeholder')
			}
		}).blur(function(){
			var input=$(this);
			if(input.val()==''||input.val()==input.attr('placeholder')){
				input.addClass('placeholder');
				input.val(input.attr('placeholder'))
			}
		}).blur()
	}



	// offset
	var bod_offset = function(){
		var offset = $(window).scrollTop();
		if(offset>100){
			$('body').addClass('offset_100');
		} else {
			$('body').removeClass('offset_100');
		}
	}
	bod_offset();
	$(window).scroll(function(){
		bod_offset();
	});



	// iCheck
	$('input[type="checkbox"],input[type="radio"]').iCheck();



	// selectBoxIt
	$('select:not([multiple])').selectBoxIt();

	

	// basictable
	$('table.normal').each(function(index, el) {
		// vetsi tabulka - povol zalamovani
		if ($(this).find('th').length > 1)
		{
			$(this).basictable({
				tableWrapper: true
			});
		}
		// mensi tabulka - nastav breakpoint na maly rozmer
		else
		{
			$(this).basictable({
				tableWrapper: true,
				breakpoint: 120 // css media query
			});
		}
	});



	/* funkce vykonane na pripravenost dokumentu */
	onDocumentReady($);



	/* vycka, nez je dokoncena jakakoliv udalost */
	var waitForFinalEvent = (function () {
		var timers = {};
		return function (callback, ms, uniqueId) {
			if (!uniqueId) {
				uniqueId = "Don't call this twice without a uniqueId";
			}
			if (timers[uniqueId]) {
				clearTimeout (timers[uniqueId]);
			}
			timers[uniqueId] = setTimeout(callback, ms);
		};
	})();



	/* na opravdovou zmenu velikosti okna - konej */
	window._width = $(window).width();
	$(window).resize(function () {
		waitForFinalEvent(function(){

			// detekce skutecne zmeny
			if (window._width != $(window).width())
			{
				window._width = $(window).width();
				onWindowResize($);
			}

		}, 500, "afterWindowResize");
	});

});


/**
 * funkce vykonane na pripravenost dokumentu
 *
 * @param   {function}  jQ  objekt jQuery
 * 
 * @return
 */
function onDocumentReady(jQ)
{
	uniteHeights(jQ); // vyrovnej vysky elementu
	return;
}


/**
 * funkce vykonane na responzivni chovani
 *
 * @param   {function}  jQ 	objekt jQuery
 *
 * @return
 */
function onWindowResize(jQ)
{
	uniteHeights(jQ);				// vyrovnej vysky elementu
	return;
}


/**
 * Funkce pro sjednocovani elementu stranky
 * - na zaklade tridy (unite-heights)
 * a nastavene ID skupiny elementu (data-ug)
 *
 * @param   {function}  jQ 	objekt jQuery
 *
 * @return
 */
function uniteHeights(jQ)
{
	// seznam skupin
	var u_gs = [];
	jQ('.unite-heights').each(function(i, el) {
		var c_ug = jQ(this).attr('data-ug');
		if (jQ.inArray( c_ug, u_gs ) == -1)
		{
			u_gs.push(c_ug);
		}
	});

	// projdi vsechny skupiny a nastav jim stejne vysky
	if (u_gs.length > 0)
	{
		jQ.each(u_gs, function(i, g_id) {

			var heighest = 0;
			s = '[data-ug="' + g_id + '"]';	// data unite group

			// zresetuj a nacti nejvyssi
			jQ(s).each(function(index, el) {
				jQ(this).css('height', 'auto');	// reset
				heighest = jQ(this).height() > heighest ? jQ(this).height() : heighest;
			});

			// nastav nejvyssi
			jQ(s).each(function(index, el) {
				jQ(this).height(heighest);
			});
		});
	}
	return;
}